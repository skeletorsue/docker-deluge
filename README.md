# Supported tags and respective ```Dockerfile``` links

* [```latest``` ```1.3.12``` (*Dockerfile*)][1]

# What is Deluge?
[Deluge](http://deluge-torrent.org/) is a lightweight, **Free Software**, **cross-platform** BitTorrent client.

# How do use this image
```docker run -d -p 58846:58846 -p 62958:62958 -p 62958:62958/udp -v /DownloadDir:/data/downloads -v /ConfigDir:/config skeletorsue/deluge```

[1]: https://bitbucket.org/skeletorsue/docker-deluge/src/master/Dockerfile?fileviewer=file-view-default
