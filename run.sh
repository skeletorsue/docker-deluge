#!/usr/bin/env bash
set -e

# Remove old pid if it exists
[ -f /config/deluged.pid ] && rm -f /config/deluged.pid

[ ! -f /config/core.conf ] && cp -rp /core.conf.def /config/core.conf

# Start Deluge Daemon
/usr/bin/deluged --config /config -d --loglevel info

# Start Deluge Web UI
#/usr/bin/deluge-web --config /config --logfile /config/deluge-web.log --loglevel info

deluge-console "config -s allow_remote True"
